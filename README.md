CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Optional email allow anonymous user to register without using email address.

INSTALLATION
------------

Install the Optional email module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

CONFIGURATION
-------------

* To configure this module goto Configuration > Account settings > Enable optional email.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Sandeep Jangra - https://www.drupal.org/u/sandeep_jangra
